import React from 'react';
import { 
	StyleSheet, 
	Text, 
	View,
	TouchableOpacity,
	Alert,
	Button,
	Image  } from 'react-native';


export default class App extends React.Component {

constructor(props) {
	super(props);

	this.state = {
		cards: [],
		player: [],
		pcd: {color: 0, number: 0},
		pcbhd: {},
		pcbnd: {},
		computer: [],
		ccd: {color: 0, number: 0},
		ccbhd: {},
		ccbnd: {},
		gameended: false
	};
}

	componentDidMount(){
		//generate cards
		var cardsinpack = 52;
		var cardsincolor = 13;
		var color = 1;
		var cardnumber = 1;
		var tempcards = []
		for (var i = 0; i < cardsinpack; i++) {
			if (cardnumber >=14) {
				cardnumber = 1
				color = color + 1
				tempcards.push({id: i, color: color, cardnumber: cardnumber});
				cardnumber = cardnumber + 1;
			}else{
				tempcards.push({id: i, color: color, cardnumber: cardnumber});
				cardnumber = cardnumber + 1;
			}
		}
		this.setState({
			cards: tempcards
		})
	}
	startgame = () =>{
	var tempitem
	var tempcards = [...this.state.cards]
	var tempplayer= []
	var tempcomputer = []

	for (var i = 0; i < 52; i++) {
		randnumb = Math.floor(Math.random()*(52-i))
		tempitem = tempcards[randnumb]
		tempcards.splice(randnumb, 1)
		randplayer = Math.floor(Math.random()*2)
		if (randplayer == 0) {
			if (tempplayer.length == 26) {
				tempcomputer.push(tempitem)
			}else{
				tempplayer.push(tempitem)
			}
		}else if (randplayer == 1) {
			if (tempcomputer.length == 26) {
				tempplayer.push(tempitem)
			}else{
				tempcomputer.push(tempitem)
			}
		}
	}
	console.log("gra rozpoczęta")
	this.setState({
			player: tempplayer,
			computer: tempcomputer,
			gameended: false
		})
	}
		
remis = () =>{
			if(tempplayer.length >= 2 && tempcomputer.length >= 2){
				tempcardhostpl = tempplayer[0]
				tempcardhostcom = tempcomputer[0]
				tempcardatpl = tempplayer[1]
				tempcardatcom = tempcomputer[1]
				this.setState({
					pcbhd: tempcardhostpl,
					pcbnd: tempcardatpl,
					ccbhd: tempcardhostcom,
					ccbnd: tempcardatcom
				})
				tempplayer.splice(0,2)
				tempcomputer.splice(0,2)
				if(tempcardatpl.cardnumber > tempcardatcom.cardnumber){
					tempplayer.push(tempcardatpl)
					tempplayer.push(tempcardhostpl)
					tempplayer.push(tempcardpl)
					tempplayer.push(tempcardatcom)
					tempplayer.push(tempcardhostcom)
					tempplayer.push(tempcardcom)
					console.log("wygrał gracz")
					this.setState({
						player: tempplayer,
						computer: tempcomputer
					})
				}else if (tempcardatpl.cardnumber < tempcardatcom.cardnumber) {
					tempcomputer.push(tempcardatcom)
					tempcomputer.push(tempcardhostcom)
					tempcomputer.push(tempcardcom)
					tempcomputer.push(tempcardatpl)
					tempcomputer.push(tempcardhostpl)
					tempcomputer.push(tempcardpl)
					console.log("wygrał komputer")
					this.setState({
						player: tempplayer,
						computer: tempcomputer
					})
				}else{
					this.remis()
				}	
			}else{
				if (tempplayer.length < 2) {
					Alert.alert(
					  'Przegrana',
					  'Czy chcesz rozpocząć nową grę?',
					  [
					    {
					      text: 'Nie',
					      onPress: () => {
					      	console.log('Nie chcę grać')
					      	this.setState({
					      		gameended: true
					      	})
					      },
					      style: 'cancel',
					    },
					    {text: 'Tak', onPress: () => {
					    	console.log('Chcę grać jeszcze raz')
					    	this.setState({
					      		gameended: true
					      	})
					    	this.startgame()
					    }},
					  ],
					  {cancelable: false},);
				}else if (tempcomputer.length < 2) {
					Alert.alert(
					  'Wygrana',
					  'Czy chcesz rozpocząć nową grę?',
					  [
					    {
					      text: 'Nie',
					      onPress: () => {
					      	console.log('Nie chcę grać')
					      	this.setState({
					      		gameended: true
					      	})
					      },
					      style: 'cancel',
					    },
					    {text: 'Tak', onPress: () => {
					    	console.log('Chcę grać jeszcze raz')
					    	this.setState({
					      		gameended: true
					      	})
					    	this.startgame()
					    }},
					  ],
					  {cancelable: false},);
				}
			}
		}


	battle = () =>{
		tempplayer = [...this.state.player]
		tempcomputer = [...this.state.computer]
		tempcardpl = tempplayer[0]
		tempcardcom = tempcomputer[0]
		this.setState({
			pcbhd: {},
			pcbnd: {},
			ccbhd: {},
			ccbnd: {},

			ccd: tempcomputer[0],
			pcd: tempplayer[0]
		})
		tempplayer.splice(0,1)
		tempcomputer.splice(0,1)
		if(typeof tempplayer === 'undefined' || tempplayer.length == 0){
			Alert.alert(
				  'Przegrana',
				  'Czy chcesz rozpocząć nową grę?',
				  [
				    {
				      text: 'Nie',
				      onPress: () => {
				      	console.log('Nie chcę grać')
				      	this.setState({
				      		gameended: true
				      	})
				      },
				      style: 'cancel',
				    },
				    {text: 'Tak', onPress: () => {
				    	console.log('Chcę grać jeszcze raz')
				    	this.setState({
				      		gameended: true
				      	})
				    	this.startgame()
				    }},
				  ],
				  {cancelable: false},
				);
		}else if (typeof tempcomputer === 'undefined' || tempcomputer.length == 0) {
			Alert.alert(
				  'Wygrana',
				  'Czy chcesz rozpocząć nową grę?',
				  [
				    {
				      text: 'Nie',
				      onPress: () => {
				      	console.log('Nie chcę grać')
				      	this.setState({
				      		gameended: true
				      	})
				      },
				      style: 'cancel',
				    },
				    {text: 'Tak', onPress: () => {
				    	console.log('Chcę grać jeszcze raz')
				    	this.setState({
				      		gameended: true
				      	})
				    	this.startgame()
				    }},
				  ],
				  {cancelable: false},
				);
		}else{
			if (tempcardpl.cardnumber > tempcardcom.cardnumber) {
				tempplayer.push(tempcardpl, tempcardcom)
				console.log('wygrał gracz')
				this.setState({
					player: tempplayer,
					computer: tempcomputer
				})
			}else if (tempcardpl.cardnumber < tempcardcom.cardnumber) {
				tempcomputer.push(tempcardcom,tempcardpl)
				console.log('wygrał komputer')
				this.setState({
					player: tempplayer,
					computer: tempcomputer
				})
			}else{
				console.log('remis')
				this.remis()
			}
		}
	}

checkcards = () =>{
	console.log("ilość kart gracza: "+ this.state.player.length)
	console.log("ilość kart Komputera: "+ this.state.computer.length)
}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.computerhand}>
				<Text style={styles.legend}>Gracz 2/Komputer</Text>
				<View style={styles.computercards}>
				{(() => {
					switch(this.state.ccd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				{(() => {
					switch(this.state.ccbhd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				{(() => {
					switch(this.state.ccbnd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.ccbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				</View>
			</View>
			  <Button
				  onPress={this.battle}
				  title="Battle"
				  color="#841584"
				/>
			  <Button 
			  	onPress={this.startgame}
			  	title="start new game"
			  	/>

			  	<View style={styles.playerhand}>
			  	<Text style={styles.legend}>Gracz 1</Text>
			  	<View style={styles.playercards}>
			  	{(() => {
					switch(this.state.pcd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				{(() => {
					switch(this.state.pcbhd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbhd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				{(() => {
					switch(this.state.pcbnd.color) {
						case 1:
							 return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/heart.png')}/>
							 	</View>
							 	);							
						case 2:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/diamond.png')}/>
							 	</View>
							 	);
						case 3:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/spades.png')}/>
							 	</View>
							 	);
						case 4:
							return(
							 	<View style={styles.card}>
							 		<Text style={styles.cardnumber}>{this.state.pcbnd.cardnumber}</Text>
							 		<Image style={styles.color} source={require('./assets/clover.png')}/>
							 	</View>
							 	);
						default:
							return null;
					}
				})()}
				</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	color: {
		width: 64,
		height: 64,
	},
	card: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	computercards: {
		flexDirection: 'row',
	},
	playercards: {
		flexDirection: 'row',
	},
	cardnumber:{
		fontSize: 64,
	},
	legend:{
		fontSize: 32,
	}
});
